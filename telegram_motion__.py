#!/usr/bin/env python3
import telepot
from picamera import PiCamera
import RPi.GPIO as GPIO
import time
from time import sleep
import datetime
from telepot.loop import MessageLoop
from subprocess import call
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("chat_id")
args = parser.parse_args()

chat_id = args.chat_id

PIR = 4
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 25

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR, GPIO.IN)

motion = 0
motionNew = 0

bot = telepot.Bot('botfather-token-here')

def main():

    global chat_id
    global motion
    global motionNew

    if GPIO.input(PIR) == 1:
        print("Motion detected")
        motion = 1
        if motionNew != motion:
            motionNew = motion
            sendNotification(motion)

    elif GPIO.input(PIR) == 0:
        #print("No motion detected")
        motion = 0
        if motionNew != motion:
            motionNew = motion

def sendNotification(motion):

    global chat_id

    if motion == 1:
        filename = "./video_" + (time.strftime("%y%b%d_%H%M%S"))
        camera.start_recording(filename + ".h264")
        sleep(5)
        camera.stop_recording()
        command = "MP4Box -add " + filename + '.h264' + " " + filename + '.mp4'
        print(command)
        call([command], shell=True)
        bot.sendVideo(chat_id, video=open(filename + '.mp4', 'rb'))
        bot.sendMessage(chat_id, 'The motion sensor is triggered!')

while 1:
    main()

while 1:
    time.sleep(10)