#!/usr/bin/env python3
import subprocess
import telepot
import time
from time import sleep
import datetime
from telepot.loop import MessageLoop

def handle(msg):
    global telegramText
    global chat_id
    global process

    chat_id = msg['chat']['id']
    telegramText = msg['text']

    print('Message received from ' + str(chat_id))

    if telegramText == '/start':
        bot.sendMessage(chat_id, 'Security camera is activated.')
        process = subprocess.Popen(['./telegram_motion__.py', str(chat_id)])

    elif telegramText == '/stop':
        bot.sendMessage(chat_id, 'Killing subprocess telegram_motion__.py')
        process.kill()

bot = telepot.Bot('botfather-token-here')
bot.message_loop(handle)

while 1:
    time.sleep(10)
