# telegram-motion-controller

## Name
Telegram-Controlled Motion-Activated PiCamera

## Description
These two python scripts will connect to a Telegram bot and can be used to control a Raspberry Pi that has a picamera and motion sensor.

## Installation
Requirements: Python 3 and
```
sudo apt-get install python-pip
sudo pip install telepot
```
Place the 2 files in the same folder on a Raspberry Pi that has a picamera and pi motion sensor attached.

Then add your Telegram bot's token into `controller.py` and `telegram_motion__.py`

Finally add a `crontab -e` entry so it starts at boot:
```
@reboot python3 /home/pi/controller.py &
```

## Usage
Plug in the raspberry pi and control the motion activation through Telegran using `/start` and `/stop` commands.

For something more advanced, you can use an Uninterruptible Power Supply and a Mobile Wifi Hotspot to still receive updates during a power outage.

## Roadmap
No immediate plans to update.

## Contributing
One of my biggest goals would be to constantly stream video into a buffer holding onto 5 seconds, and start saving video including the 5 seconds before motion is sensed and divide the stream so that
1. there's a 5 second preview that's sent to Telegram that will not include the 5 seconds before, and
2. and a longer video that keeps recording as long as motion is activated saved to disk.

So you get a 5 second preview of immediate motion activity, and a full recording with 5 second buffers on the beginning and end of the video.


It would be cool to have an rsync system to push to an external storage device other than Telegram.

Open to a shell script that turns off unnecessary Pi services to save on resources.

## Authors and acknowledgment
Modified version of https://www.cytron.io/tutorial/send-video-to-telegram-bot-using-raspberry-pi-camera by suadanwar dated 16 Oct 2020.

I added a controller for starting/stopping motion sensor on demand.

## License
Whatever.

## Project status
Sluggish
